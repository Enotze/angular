export class User {
    constructor(readonly id: number, public middleName: string, public name: string, public surname: string = '') {
    }
}