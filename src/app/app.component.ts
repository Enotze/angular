import {Component} from '@angular/core';
import {User} from './shared/user.model';

const LOCAL_STORAGE_KEY = 'users';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent {
    title = 'Панель управления пользователями';
    currentUser: User = null;
    users: User[] = [];

    constructor() {
        this.setNewCurrentUser();
        this.setUsersFromLocalStorage();
        console.log(this.users);
    }

    saveNewUser() {
        console.log(this.currentUser);
        this.users.push(this.currentUser);
        this.setNewCurrentUser();
        this.setUsersToLocalStorage();
        console.log(this.users);
    }

    setNewCurrentUser() {
        this.currentUser = new User(this.users.length, '', '');
    }

    setUsersFromLocalStorage() {
        this.users = JSON.parse(localStorage.getItem(LOCAL_STORAGE_KEY)) || [];
    }

    setUsersToLocalStorage() {
        localStorage.setItem(LOCAL_STORAGE_KEY, JSON.stringify(this.users));
    }
}
