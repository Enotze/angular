import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {FormsModule} from '@angular/forms';
import {UserFormComponent} from './user-form/user-form.component';
import {UsersListComponent} from './users-list/users-list.component';
import {UserCardComponent} from './user-card/user-card.component';

@NgModule({
    declarations: [
        AppComponent,
        UserFormComponent,
        UsersListComponent,
        UserCardComponent
    ],
    imports: [
        BrowserModule,
        FormsModule
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule {
}
