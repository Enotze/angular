import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import {User} from '../shared/user.model';

@Component({
    selector: 'app-user-form',
    templateUrl: './user-form.component.html',
    styleUrls: ['./user-form.component.css']
})
export class UserFormComponent implements OnInit {
    @Input() user: User;
    @Output() userSave: EventEmitter<User> = new EventEmitter();

    ngOnInit() {
    }

    onSaveBtnClick() {
        this.userSave.emit(this.user);
    }
}
